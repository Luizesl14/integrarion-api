import { DataSource } from 'typeorm';

const AppDataSource = new DataSource({
  type: 'postgres',
  host: 'localhost',
  port: 5436,
  username: 'postgres',
  password: 'docker',
  database: 'postgres',
  migrations: ['./src/shared/typeorm/migrations/*.ts'],
});

AppDataSource.initialize()
  .then(() => {
    console.log('[INFO] ###### Data Source has been initialized!');
  })
  .catch(err => {
    console.error('[INFO] ###### during Data Source initialization', err);
  });
